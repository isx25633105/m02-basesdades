EXAMEN BASE DE DADES

1.
SELECT descripcion,precio FROM productos WHERE descripcion ILIKE '%riostra%';

2.
SELECT oficina_rep, count(num_empl) FROM repventas WHERE edad < 50 GROUP BY oficina_rep HAVING count(num_empl) >= 2;

3.
SELECT producto, sum(cant) as "Unitats venudes", sum(importe) as "Facturación total", count(num_pedido) as "Pedidos" FROM pedidos GROUP BY producto HAVING sum(cant) > 60; 

4.
SELECT nombre, (ventas-cuota) as "Superació quota", ((ventas/cuota)*100) as "SuperacióQuotaPercentatge" FROM repventas WHERE cuota IS NOT NULL;

5.
SELECT fab FROM pedidos GROUP BY fab HAVING sum(cant) > 50;

6.
SELECT id_fab FROM productos WHERE (precio <= 1000 and precio >= 200) and (descripcion LIKE '%or' or descripcion LIKE '%ra') and (id_producto LIKE '%7%' or id_producto LIKE '%8%') GROUP BY id_fab;

7.
SELECT fab, rep, sum(cant) FROM pedidos GROUP BY fab, rep ORDER BY fab, sum(cant) DESC;

8.
SELECT director FROM repventas GROUP BY director ORDER BY count(num_empl) DESC LIMIT 2;

9.
SELECT clie FROM pedidos GROUP BY clie HAVING sum(importe) > 35000 and count(num_pedido) > 1;

10.
SELECT producto, count(num_pedido) FROM pedidos WHERE fab LIKE 'fea' or fab LIKE 'rei' GROUP BY producto HAVING count(num_pedido) > 1;


