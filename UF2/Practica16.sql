--  1. Llistar els productes amb existències no superiors a 200 i no inferiors a 20 de la fàbrica aci o de la fabrica imm, i els nom dels clients que han comprat aquests productes. Si algun producte no l'ha' comprat ningú ha de sortir sense nom de client.
       select descripcion 
       from productos
       where existencias <_ 200 and existencias >_ 20 and (id_fab ilike 'aci' or id_fab ilike 'imm')
       
       UNION 
       select NULL, empresa
       from 
       
--  2. Llistar la ciutat de cada oficina, el número total de treballadors, el nom del cap de l''oficina i el nom de cadascun dels treballadors (incloent al cap com a treballador).
       Select ciudad, count(repventas.nombre) as "Total treballadors",
       a.nombre as "cap" , NULL as "Treballadors"
       from oficinas
       join repventas on oficina=oficina_rep
       join repventas as a on a.num_empl=dir
       Group by ciudad, a.nombre
       UNION
       Select ciudad, Null, Null, nombre 
       from oficinas join repventas on oficina=oficina_rep
       UNION
       select ciudad, NULL, NULL, nombre
       from oficinas join repventas on num_empl =dir
       Order by 1,2;
       -------------+--------------------+-------------+---------------
 Atlanta     |                  1 | Bill Adams  | 
 Atlanta     |                    |             | Bill Adams
 Chicago     |                  3 | Bob Smith   | 
 Chicago     |                    |             | Paul Cruz
 Chicago     |                    |             | Bob Smith
 Chicago     |                    |             | Dan Roberts
 Denver      |                  1 | Larry Fitch | 
 Denver      |                    |             | Larry Fitch
 Denver      |                    |             | Nancy Angelli
 Los Angeles |                  2 | Larry Fitch | 
 Los Angeles |                    |             | Sue Smith
 Los Angeles |                    |             | Larry Fitch
 New York    |                  2 | Sam Clark   | 
 New York    |                    |             | Sam Clark
 New York    |                    |             | Mary Jones
(15 rows)

       
       
--  3.Llistar els noms i preus dels productes de la fàbrica imm i de la fàbrica rei que contenen 'gr' o 'tr' en el seu nom i que valen entre 900 i 5000€, i els noms dels venedors que han venut aquests productes. Si algun producte no l'ha comprat ningú ha de sortir sense nom venedor.
select 'Productos: ', descripcion as Producto, precio, NULL as nombre
from productos
where (descripcion like '%gr%' or descripcion like '%tr%') and (precio >= 900 and precio <= 5000) UNION SELECT 'Vendedores: ', descripcion, NULL, nombre
from repventas 
join pedidos ON num_empl=rep
left join productos on id_producto=producto
where (descripcion LIKE '%gr' or descripcion LIKE '%tr%') AND (precio >= 900
AND precio <= 5000) ORDER BY 2,1;

    --4. Llistar els noms dels productes, el número total de ventes que s'ha fet d'aquell producte, la quantitat total d'unitats que s'han venut d'aquell producte, i el nom de cada client que l'ha comprat.
Select 'Productos: ', descripcion, count(producto), sum(cant), NULL as Empresa
from productos
left join pedidos on id:producto=producto
group by descripcion
UNION
Select '     Client: ', descripcion, NULL, NULL, empresa
from clientes
join pedidos on num_clie=clie; 

-- 5. Llistar els productes que costen més de 1000 o no són ni de la fàbrica imm ni de la fàbrica rei, ni de la fàbrica ací, i el total que n'ha comprat cada client. Si algun producte no l'ha comprat ningú ha de sortir sense nom de client.
select 'Productos:' as tipus, descripcion, null as empresa, null as total
from pedidos right join productos
on fab=id_fab and producto=id_producto
where productos.precio>1000
or (not fab='aci' or not fab='rei' or not fab='imm')

Union

select '   Client:', descripcion,  empresa, sum(importe) 
from pedidos right join productos on fab=id_fab and producto=id_producto
join clientes
on clie=num_clie
where productos.precio>1000
or (not fab='aci' or not fab='rei' or not fab='imm')
group by clientes.empresa, descripcion
order by descripcion;
    --6. Llistar els codis de fabricants, el número total de productes d'aquell fabricant i el nom de cadascun dels productes.
select 'Fabricant:', id_fab, count(*) as "Total productes", NULL as descripcion
from productos
group by id_fab

Union
select ' Descripcióm:', id_fab, NULL, descripcion
from productos

order by id_fab, "Total productes";

    --7. Llistar els venedors i els seus imports totals de ventes, que tinguin més de 30 anys i treballin a l'oficina 12 i els que tinguin més de 20 anys i treballin a l'oficina 21. Llistar els clients a qui ha venut cadascun d'aquests venedors
select 'Vendedor:', nombre as "Representat/client", ventas, NULL
from repventas
where (edad > 30 and oficina_rep=12) or (edad > 20 and oficina_rep=21)

union 

select '   Client:', empresa, NULL
from 
repventas 
left join pedidos on num_empl=rep
join clientes on clie=num_clie 
where (edad > 30 and oficina_rep=12) or (edad > 20 and oficina_rep=21)

order by "Representant/client", 1 desc;
