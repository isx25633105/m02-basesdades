1. Visualitza limport total i el número de comandes per producte que shan fet dels productes que tenen un codi que comneça per 4 i acaba en 2, en 3 o en 4 i dels quals shan fet 3 o més comandes.

	SELECT PRODUCTO, sum(importe), count(num_pedido) 
	from pedidos 
	where ((producto LIKE '4%') AND (producto like '%2' or producto like '%3' or producto like '%4')) 
	GROUP BY PRODUCTO 
	having count(num_pedido) >=3;

2. Volem saber quants clients diferents ens han fet comandes de més de 7 unitats de productes de les fàbriques aci i rei

	SELECT count(distinct (clie)) as "Quantitat de clients" 
	from pedidos
	WHERE ((fab ilike 'REI' or fab ilike 'ACI' ) AND cant > 7 );
	
	
3. Estem interessats en els clients diferents ens han fet comandes de més de 7 unitats de productes de les fàbriques aci i rei. Volem saber quins codis de clients ens han fet dues o més comandes daquestes característiques.
	
	SELECT distinct clie as "Clients" 
	from pedidos
	WHERE ((fab ilike 'REI' or fab ilike 'ACI' ) AND cant > 7 )
	group by clie
	having min (clie) >= 2
	;
	
4. Volem saber quin fabricant és el que té més unitats dels seus productes als nostres magatzems. Visualitza el codi de la fàbrica i el total dunitats dels seus productes que tenim.

	

5. Entre els nostres productes nhi ha que tenen el mateix nom(descripcion). Visualitza la descripcion daquests productes I el preu del més car.


6. Volem saber a quines oficines treballen venedors amb un cognom que comenci per A o per S I un nom que comenci amb T o amb S o amb B o amb N. També volem saber quants trebalaldors daquestes característiqes hi treballen I quina és la suma de les seves quotes per oficina. No volem que es visualitzi el Tom Snyder perquè ell no pertany a cap oficina en particular.

	SELECT oficina_rep, count(num_empl), sum(cuota) 
    from repventas 
    where (nombre ilike '% a%' or nombre ilike '% s%')and(nombre ilike 't%' or nombre ilike 'S%' or nombre ilike 'b%' or nombre ilike 'n%')and(oficina_rep is not null)
    group by oficina_rep
    ;

7. Quins són els codis dels representants que tenen assignats més de 2 clients amb un nom dempresa que contingui la lletra o I la lletra I?

    SELECT rep_clie
    from clientes
    where empresa ilike '%o%' and empresa ilike '%i%' 
    group by rep_clie
    having count (num_clie) >2
    ;

8. Quins codis de clients ens han comprat 3 o més vegades?

    SELECT clie
    from pedidos
    group by clie
    having count(num_pedido) >=3
    ;

9. Quins codis de clients ens han comprat més duna vegada el mateix producte?

    SELECT clie,
    from pedidos
    having count(producto)>1
    ;

    SELECT CLIE, count(producto) 
    FROM pedidos 
    group by clie
    ;
10. Dentre els productes que shan venut més duna vegada, quin és els que ens ha donat un total dimport més baix? Visualitza el codi del producta, la quantitat de cops que sha venut I el total dimport que sha cobrat per aquest producte.

    SELECT producto, count(producto), sum(importe) as suma
    from pedidos
    group by producto
    order by suma ASC limit 1
    ;