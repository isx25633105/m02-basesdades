- 9. Per les oficines mostrar l'identificador i la ciutat de l'oficina. Llistar les 3 oficines que entre tots els seus treballadors han fet més comandes. 


-- 10. Dels productes mostrar el preu i un camp anomenat "cantidad". Per cada producte, el camp "cantidad" ha de ser el total de les quantitats demanades a les comandes. Només mostrar els productes que s'hagin demanat a menys de 2 comandes. 


-- 11. Llistar aquells clients amb un import de comandes acumulat més gran a 10000. Mostrar l'identificador del client i el nom dels representants de vendes del client. 


-- 12. Em quant a les existències dels productes: Mostrar un camp anomenat "m_e". El camp "m_e" ha de contenir el valor de les existències del producte amb menys existències. No s'han de tenir en compte els productes del fabricant amb identificació "bic" ni aquells productes que la seva descripció comenci per "Riostra". 


-- 13. Mostrar l'identificador i el nom del representant de vendes i un camp anomenat "lim". El camp "lim" ha de mostrar la mitja del límit de crèdit dels clients representats per cada un dels representants de vendes. 


-- 14. Llistar els directors d'empleats. Mostrar l'identificador, el nom i l'edat dels representants de vendes que són director d'empleats. També mostrar un camp anomenat "e_sub". El camp "e_sub" ha de mostrar la mitja d'edat del empleats assignats a cada un dels directors. 


-- 15. Mostrar l'identificador i el nom dels representants de vendes seguit d'un camp anomenat "pedidos". El camp "pedidos" ha de mostrar la quantitat de comandes que ha fet cada representat de vendes. No s'ha de tenir en compte aquells representants de vendes que no tinguin un director de vendes associat. S'han de mostrar ordenats en primera instància del representant de vendes que té més comandes al que te menys comandes, i en segona instància per ordre alfabètic del nom del treballador. 

