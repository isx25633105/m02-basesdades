-- 1. Per a cada comanda. mostra el nom del producte i la ciutat des de la qual s'ha venut.
training=# select descripcion, ciudad from productos, pedidos, oficinas, repventas where rep=dir and  fab=id_fab and  producto=id_producto and oficina=oficina_rep  ; 
   descripcion   |   ciudad    
-----------------+-------------
 Articulo Tipo 2 | Atlanta
 Articulo Tipo 3 | Atlanta
 Articulo Tipo 4 | Atlanta
 Articulo Tipo 4 | Atlanta
 Extractor       | Atlanta
 Bancada Motor   | New York
 Bisagra Izqda.  | New York
 Articulo Tipo 2 | Los Angeles
 Manivela        | Los Angeles
 Riostra 1/2-Tm  | Los Angeles
 Reductor        | Los Angeles
 Reductor        | Los Angeles
 Bisagra Dcha.   | Los Angeles
 Bancada Motor   | New York
 Bisagra Izqda.  | New York
 Articulo Tipo 2 | Los Angeles
 Manivela        | Los Angeles
 Riostra 1/2-Tm  | Los Angeles
 Reductor        | Los Angeles
 Reductor        | Los Angeles
 Bisagra Dcha.   | Los Angeles
 Articulo Tipo 2 | Denver
 Manivela        | Denver
 Riostra 1/2-Tm  | Denver
 Reductor        | Denver
 Reductor        | Denver
 Bisagra Dcha.   | Denver
(27 rows)


-- 2. Per a cada client, mostra el seu nom i quantes comandes ha fet.
training=# select empresa, count(num_pedido) from clientes, pedidos where num_clie=clie group by empresa;
      empresa      | count 
-------------------+-------
 First Corp.       |     1
 Zetacorp          |     2
 Peter Brothers    |     2
 Midwest Systems   |     4
 Holm & Landis     |     3
 JCP Inc.          |     3
 Fred Lewis Corp.  |     2
 J.P. Sinclair     |     1
 Ace International |     2
 Orion Corp        |     2
 Rico Enterprises  |     1
 Acme Mfg.         |     4
 Jones Mfg.        |     1
 Ian & Schmidt     |     1
 Chen Associates   |     1
(15 rows)

-- 3. Per a cada representatn, volem veure quins de clients ha gestionat comandes. Llista-ho ordenat per representant.
select nombre, empresa from (repventas left join pedidos on num_empl=rep left join clientes on clie=num_clie)
order by nombre;
    nombre     |      empresa      
---------------+-------------------
 Bill Adams    | Acme Mfg.
 Bill Adams    | JCP Inc.
 Bill Adams    | Acme Mfg.
 Bill Adams    | Acme Mfg.
 Bill Adams    | Acme Mfg.
 Bob Smith     | 
 Dan Roberts   | Ian & Schmidt
 Dan Roberts   | First Corp.
 Dan Roberts   | Holm & Landis
 Larry Fitch   | Orion Corp
 Larry Fitch   | Midwest Systems
 Larry Fitch   | Zetacorp
 Larry Fitch   | Midwest Systems
 Larry Fitch   | Midwest Systems
 Larry Fitch   | Zetacorp
 Larry Fitch   | Midwest Systems
 Mary Jones    | Holm & Landis
 Mary Jones    | Holm & Landis
 Nancy Angelli | Peter Brothers
 Nancy Angelli | Peter Brothers
 Nancy Angelli | Chen Associates
 Paul Cruz     | JCP Inc.
 Paul Cruz     | JCP Inc.
 Sam Clark     | J.P. Sinclair
 Sam Clark     | Jones Mfg.
 Sue Smith     | Fred Lewis Corp.
 Sue Smith     | Rico Enterprises
 Sue Smith     | Orion Corp
 Sue Smith     | Fred Lewis Corp.
 Tom Snyder    | Ace International
 Tom Snyder    | Ace International
(31 rows)

-- 4. Mostra el nom de cada representant i el del seu director. Fes que els noms dels camps siguin significatius.
