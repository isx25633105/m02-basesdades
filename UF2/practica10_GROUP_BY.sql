    1. Visualitza l'import total i el número de comandes per producte que s'han fet dels productes que tenen un codi que comneça per 4 i acaba en 2, en 3 o en 4 i dels quals s'han fet 3 o més comandes.

	SELECT producto, sum(importe), count(num_pedido) from pedidos where ((producto LIKE '4%') and (producto like '%2' or producto like '%3' or producto like '%4)) GROUP by PRODUCTO having COUNT(num_pedido) >==3;
    2. Volem saber quants clients diferents ens han fet comandes de més de 7 unitats de productes de les fàbriques aci i rei
	
SELECT count(distinct clie) 
FROM pedidos
where ((cant > 7) and (fab like 'rei' or  fab  like 'aci'));
 count 
-------
     7
(1 row)

    3. Estem interessats en els clients diferents ens han fet comandes de més de 7 unitats de productes de les fàbriques aci i rei. Volem saber quins codis de clients ens han fet dues o més comandes d'aquestes característiques.
training=# SELECT distinct  clie  
FROM pedidos
where ((cant > 7) and (fab like 'rei' or  fab  like 'aci')) group by clie  having count(num_pedido) >= 2;
 clie 
------
 2103
 2107
 2111
(3 rows)


    4. Volem saber quin fabricant és el que té més unitats dels seus productes als nostres magatzems. Visualitza el codi de la fàbrica i el total d'unitats dels seus productes que tenim.
     SELECT id_fab sum(existencias

    5. Entre els nostres productes n'hi ha que tenen el mateix nom(descripcion). Visualitza la descripcion d'aquests productes I el preu del més car.
training=# SELECT DESCRIPCION, max(precio) from productos GROUP by DESCRIPCION having count(descripcion)>1
;
 descripcion |  max   
-------------+--------
 Reductor    | 355.00
(1 row)


    6. Volem saber a quines oficines treballen venedors amb un cognom que comenci per A o per S I un nom que comenci amb T o amb S o amb B o amb N. També volem saber quants trebalaldors d'aquestes característiqes hi treballen I quina és la suma de les seves quotes per oficina. No volem que es visualitzi el Tom Snyder perquè ell no pertany a cap oficina en particular.
SELECT oficina_rep, count(num_empl as "RecompteEmleats", sum(couta) as SumaQuotes from repventas where (nombre ilike '% a %

    7. Quins són els codis dels representants que tenen assignats més de 2 clients amb un nom d'empresa que contingui la lletra o I la lletra I?
SELECT REP_CLIE from clientes where empresa ilike '%o%' and empresa ilike '%i%' group by rep_clie having count (num_clie) >2;

    8. Quins codis de clients ens han comprat 3 o més vegades?
SELECT clie from pedidos group by clie having count(num_pedido) >=3:

    9. Quins codis de clients ens han comprat més d'una vegada el mateix producte?
SELECT CLIE FROM pedidos group by clie, producto having count(producto)>1:

    10. D'entre els productes que s'han venut més d'una vegada, quin és els que ens ha donat un total d'import més baix? Visualitza el codi del producta, la quantitat de cops que s'ha venut I el total d'import que s'ha cobrat per aquest producte.
