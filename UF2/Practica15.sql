    1. Mostar les ventes individuals dels productes dels fabricants 'aci' i 'rei' que comencin per 'Bisagra' o 'Articulo'. Mostrar també el total venut d''aquests productes.
       Select descripcion, producto, fab, cant, importe, fecha_pedido as "data"
       From pedidos left join productos on fab=id_fab and producto=id_producto where (fab ilike 'aci' or fab ilike 'rei')
       and (descripcion ilike 'Bisagra%' or descripcion ilike 'Articulo%')
       Union
       Select 'TOTAL:', producto, fab, sum(cant), sum(importe), current_timestamp
       from pedidos left join productos on fab=id_fab and producto=id_producto  on fab=id_fab and producto=id_producto
       where (fab ilike 'aci' or fab ilike 'rei')
       and (descripcion ilike 'Bisagra%' or descripcion ilike 'Articulo%')
       group by descripcion,fab,producto
       order by fab, producto, "data";
    2. Mostrar les ventes individuals fetes pels venedors de New York i de Chicago que superin els 2500€. Mostrar també el total de ventes de cada oficina.
    select 'venta -->' as "operacio", ciudad, num_pedido, cant, fab, producto, importe
    from pedidos
    join repventas on rep=num_empl
    join oficinas on oficina_rep=oficina
    where importe>2500 and (oficina=11 or oficina=12)
    UNION
    
    select 'TOTAL -->' as "Pepitu", ciudad, NULL, sum(cant), NULL, NULL, sum(importe)
    from pedidos
    join repventas as rep=num_empl
    join oficinas on oficina_rep=oficina
    where importe>2500 and (oficina=11 or oficina=12)
    group by ciudad 
    order by 2
    ;
    operacio  |  ciudad  | num_pedido | cant | fab | producto | importe  
-----------+----------+------------+------+-----+----------+----------
 TOTAL --> | Chicago  |            |   39 |     |          | 26478.00
 venta --> | Chicago  |     112968 |   34 | aci | 41004    |  3978.00
 venta --> | Chicago  |     113042 |    5 | rei | 2a44r    | 22500.00
 TOTAL --> | New York |            |   10 |     |          | 37125.00
 venta --> | New York |     112961 |    7 | rei | 2a44l    | 31500.00
 venta --> | New York |     113003 |    3 | imm | 779c     |  5625.00
(6 rows)

    3. Mostrar quantes ventes ha fet cada venedor, la mitjana de numero de ventes dels venedors de cada oficina i el numero de ventes total.
    Select 'vendes:', nombre, ciudad, repventas.ventas 
    from repventas
    left join oficinas
    on (oficina_rep=oficina)
    union 
    select 'media: ', NULL, ciudad, avg(repventas.ventas) from oficinas 
    right join repventas
    on (oficina=oficina_rep)
    group by ciudad
    union 
    
    select 'Total: ', NULL, NULL, sum(repventas.ventas) 
    from repventas
    
    order by ciudad;
    4. Mostrar les compres de productes de la fabrica 'aci' que han fet els clients del Bill Adams i el Dan Roberts. Mostrar també l''import total per cada client.
    select num_pedido, descripcion, fab, cant, importe, empresa, nombre
    from pedidos
    left join productos on fab=id_fab and producto=id_producto 
    join clientes on (clie=num_clie)
    join repventas on (rep_clie=num_empl)
    where fab ilike 'aci' and (num_empl=103 or num_empl=101)
    union 
    
    select 'total client:', NULL, NULL, NULL, NULL, sum(importe), empresa, NULL
    from pedidos join clientes on (clie=num_clie)
    join repventas on (rep_clie=num_empl)
    where fab ilike 'aci' and (num_empl=105 or num_empl=101)
    group by empresa
    order by empresa,concepte;
    5. Mostrar el total de ventes de cada oficina i el total de ventes de cada regió
    select 'ventas por oficina:' as "concepte al qual m'estic refetint", ciudad, region, ventas
    from oficinas
    
    union
    
    select 'vetnas por region:', NULL, region, sum(ventas)
    from oficinas
    group by region
    order by region, 1;
    6. Mostrar els noms dels venedors de cada ciutat i el numero total de venedors per ciutat
    select 'vendedor:' as concepte, nombre, ciudad, NULL as total
    from repventas join oficinas as
    7. Mostrat els noms dels clients de cada ciutat i el numero total de clients per ciutat.
    8. Mostrat els noms dels treballadors que son -caps- d''algú, els noms dels seus -subordinats- i el numero de treballadors que té assignat cada cap.
