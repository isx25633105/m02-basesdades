    1. Quants representants de ventes té cada oficina?. Visualitza el codi d’oficina, el nom de la ciutat, el nom del cap de l’oficina i el número de treballadors assignats.
select oficina, ciudad, nombre as Director_Oficina,(select count(num_empl) 
from repventas 
where oficina_rep =oficina)
from oficinas left join repventas on dir=num_empl; 

    2. Mostra el nom dels clients que han comprat productes de les fàbriques amb un codi que contingui la lletra 'i' o la lletra 'a' però no la lletra 'm'.
       select distinct(empresa)
       from clientes join pedidos on clie=num_clie
       where (fab ilike '%i%' or fab ilike '%a%') and not fab ilike '%m%';

    3. Mostra els noms dels productes que han comprat els clients que tenen dos espais en blanc en el seu nom.
	   select descripcion, empresa 
	   from productos join pedidos on fab=id_fab and producto=id_producto
	   join clientes on clie=num_clie
	   where empresa ilike '% % %';


    4. Per cada proveïdor mostra el total d'import' de ventes que ha facturat el director de la seva oficina dels productes de la fabrica 'imm' amb qualsevol preu o dels productes qualsevol altra fàbrica si tenen un preu superior a 75 i inferior a 300. Cal que surtin tots els proveïdors, tant si tenen director com si no. 
		select nombre, dir, importe,
		select dir, coalesce(
		(select sum(importe)
		from pedidos
		join productos on fab=id_fab and producto=id_producto where  (id_fab  


    5. Mostra el nom del producte de cadascuna de les comandes, la data de comanda, l'import i el nom de la ciutat de l'oficina que ha fet la venta. Ordena la sortida per ciutat i per data de comanda.
		select descripcion, fecha_pedido, importe, ciudad 
		from pedidos 
		left join productos on fab=id_fab and producto=id_producto
		left join repventas on rep=num_empl
		left join oficinas on oficina_rep=oficina
		order by ciudad, fecha_pedido;
