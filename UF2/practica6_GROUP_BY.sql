-- 1.- Quina és la comanda promig de cada venedor?


-- 2.- Quin és el rang de quotes asignades a cada oficina? ( es a dir, el mínim i el màxim)


-- 3.- Quants venedors estan asignats a cada oficina?


-- 4.- Quants clients són atesos per (tenen com a representant oficial a) cada cada venedor?


-- 5.- Calcula el total de l'import de les comandes per cada venedor i per cada client.


-- 6.- El mateix que a la qüestió anterior, però ordenat per client i dintre de client per venedor.


-- 7.- Calcula les comandes (imports) totals per a cada venedor.


-- 8.- Quin és l'import promig de les comandes per cada venedor, les comandes dels quals sumen més de 30 000?


-- 9.- Per cada oficina amb 2 o més persones, calculeu la quota total i 
-- les vendes totals per a tots els venedors que treballen a la oficina 

