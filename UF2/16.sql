    1. Llistar els productes amb existències no superiors a 200 i no inferiors a 20 de la fàbrica aci o de la fabrica imm, i els nom dels clients que han comprat aquests productes. Si algun producte no l'ha' comprat ningú ha de sortir sense nom de client.
       select descripcion 
       from productos
       where existencias <_ 200 and existencias >_ 20 and (id_fab ilike 'aci' or id_fab ilike 'imm')
       
       UNION 
       select NULL, empresa
       from 
       
    2. Llistar la ciutat de cada oficina, el número total de treballadors, el nom del cap de l''oficina i el nom de cadascun dels treballadors (incloent al cap com a treballador).
       Select ciudad, count(repventas.nombre) as "Total treballadors",
       a.nombre as "cap" , NULL as "Treballadors"
       from oficinas
       join repventas on oficina=oficina_rep
       join repventas as a on a.num_empl=dir
       Group by ciudad, a.nombre
       UNION
       Select ciudad, Null, Null, nombre 
       from oficinas join repventas on oficina=oficina_rep
       UNION
       select ciudad, NULL, NULL, nombre
       from oficinas join repventas on num_empl =dir
       Order by 1,2;
       -------------+--------------------+-------------+---------------
 Atlanta     |                  1 | Bill Adams  | 
 Atlanta     |                    |             | Bill Adams
 Chicago     |                  3 | Bob Smith   | 
 Chicago     |                    |             | Paul Cruz
 Chicago     |                    |             | Bob Smith
 Chicago     |                    |             | Dan Roberts
 Denver      |                  1 | Larry Fitch | 
 Denver      |                    |             | Larry Fitch
 Denver      |                    |             | Nancy Angelli
 Los Angeles |                  2 | Larry Fitch | 
 Los Angeles |                    |             | Sue Smith
 Los Angeles |                    |             | Larry Fitch
 New York    |                  2 | Sam Clark   | 
 New York    |                    |             | Sam Clark
 New York    |                    |             | Mary Jones
(15 rows)

       
    3. Llistar els noms i preus dels productes de la fàbrica imm i de la fàbrica rei que contenen 'gr' o 'tr' en el seu nom i que valen entre 900 i 5000€, i els noms dels venedors que han venut aquests productes. Si algun producte no l'ha' comprat ningú ha de sortir sense nom venedor.
select '----Producto------', id_fab, id_producto, descripcion, precio, NULL as nombre from productos
where id_fab= 'imm' or id_fab='rei'
and descripcion ilike '%gr"' or descripcion ilike '%tr%'
and precio between 900 and 5000
UNION
select id_fab, id_producto, precio, NULL, NULL, nombre
from productos right join pedidos on fab=id_fab and producto=id_producto
join repventas on num_empl=rep
order by id_fab, id_producto;

    4. Llistar els noms dels productes, el número total de ventes que s'ha fet d'aquell producte, la quantitat total d'unitats que s'han venut d'aquell producte, i el nom de cada client que l'ha comprat.


    5. Llistar els poductes que costen més de 1000 o no són ni de la fàbrica imm ni de la fàbrica rei, ni de la fàbrica ací, i el total que n'ha comprat cada client. Si algun producte no l'ha comprat ningú ha de sortir sense nom de client.


    6. Llistar els codis de fabricants, el número total de productes d'aquell fabricant i el nom de cadascun dels productes.


    7. Llistar els venedors i els seus imports totals de ventes, que tinguin més de 30 anys i treballin a l'oficina 12 i els que tinguin més de 20 anys i treballin a l'oficina 21. Llistar els clients a qui ha venut cadascun d'aquests venedors
