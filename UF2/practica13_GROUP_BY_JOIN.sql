-- 1. Mostrar un camp anomenat "edadm". El camp ha de contenir la mitjana de l'edat dels treballadors. 
 


-- 2. Per cada venedor (o treballador o reprentant de ventes), mostrar l'identificador del venedor i un camp anomenat "precio0". El camp "precio0" ha de contenir el preu del producte més car que ha venut. 



-- 3. Mostrar l'identificador del fabricant i un camp anomenat "m_precio". El camp "m_precio" ha de mostrar la mitjana del preu dels productes de cada fabricant. 


-- 4. Per cada client que ha fet alguna compra, mostrar l'identificador del client i un camp anomenat "pedidos". El camp "pedidos" ha de mostrar quantes comandes ha fet cada client. 




--5. Per cada client mostrar l'identificador. Només mostrar aquells clients que la suma dels imports de les seves comandes sigui menor al limit de crèdit. 
select num_clie from clientes join  pedidos on num_clie=clie  group by num_clie, limite_credito having sum(importe) < limite_credito; 
 num_clie 
----------
     2118
     2108
     2112
     2101
     2124
     2103
     2102
     2107
     2117
     2111
     2120
     2106
(12 rows)




-- 6. Mostrar els directors d'empleats. Seleccionar l'identificador, el nom i un camp anomenat "empleados". Només han d'aparèixer al llistat els directors d'empleats que tenen empleats al seu càrrec. El camp "empleados" ha de mostrar el nombre d'empleats que té assignat cada director d'empleats. 


-- 7. Mostrar l'identificador i la ciutat de les oficines i dos camps més, un anomenat "credito1" i l'altre "credito2". Per a cada oficina, el camp "credito1" ha de mostrar el límit de crèdit més petit d'entre tots els clients que el seu representant de vendes treballa a l'oficina. El camp "credito2" ha de ser el mateix però pel límit de crèdit més gran. 



-- 8. Per cada venedor i cadascun dels seus clients, mostrar l'identificador del venedor, l'identificador del client i un camp anomenat "importe_m". El camp "importe_m" ha de mostrar l'import mig de les comandes que ha realitzat cada venedor a cada client diferent. 


-
