1.Per cada oficina llista'n' la ciutat, les vendes segons la taula oficinas i les vendes segons la taula pedidos. Posa noms significatius a les dues columnes de vendes.
 training=# select oficina, ciudad, sum(importe) as pedidos_ventas, oficinas.ventas as oficinas_ventas from (repventas left join pedidos on num_empl=rep left join oficinas on oficina=oficina_rep) where oficina is not null group by oficina, ciudad;
 oficina |   ciudad    | pedidos_ventas | oficinas_ventas 
---------+-------------+----------------+-----------------
      11 | New York    |       40063.00 |       692637.00
      12 | Chicago     |       29328.00 |       735042.00
      13 | Atlanta     |       39327.00 |       367911.00
      21 | Los Angeles |       81409.00 |       835915.00
      22 | Denver      |       34432.00 |       186042.00
(5 rows)
2.Per a totes les comandes, visualitzar-ne el número de comanda, la regió de l''oficina del representatn de vendes que l''ha fet, l''import i el nom de l''empresa que ha comprat el producte.
Mostrar-ho ordenat regió. Respon a sota dels resultats: perquè hi ha dues comandes per a les quals la regió surt en blanc? 
select num_pedido, region, nombre, importe, empresa
from (pedidos left join repventas on rep=num_empl left join oficinas on oficina=oficina_rep left join clientes on clie=num_clie)
Order by region
;
 num_pedido | region |    nombre     | importe  |      empresa      
------------+--------+---------------+----------+-------------------
     113042 | Este   | Dan Roberts   | 22500.00 | Ian & Schmidt
     113012 | Este   | Bill Adams    |  3745.00 | JCP Inc.
     112989 | Este   | Sam Clark     |  1458.00 | Jones Mfg.
     112968 | Este   | Dan Roberts   |  3978.00 | First Corp.
     112963 | Este   | Bill Adams    |  3276.00 | Acme Mfg.
     113058 | Este   | Mary Jones    |  1480.00 | Holm & Landis
     112983 | Este   | Bill Adams    |   702.00 | Acme Mfg.
     113027 | Este   | Bill Adams    |  4104.00 | Acme Mfg.
     112975 | Este   | Paul Cruz     |  2100.00 | JCP Inc.
     113055 | Este   | Dan Roberts   |   150.00 | Holm & Landis
     113003 | Este   | Mary Jones    |  5625.00 | Holm & Landis
     112987 | Este   | Bill Adams    | 27500.00 | Acme Mfg.
     113057 | Este   | Paul Cruz     |   600.00 | JCP Inc.
     112961 | Este   | Sam Clark     | 31500.00 | J.P. Sinclair
     113062 | Oeste  | Nancy Angelli |  2430.00 | Peter Brothers
     112993 | Oeste  | Sue Smith     |  1896.00 | Fred Lewis Corp.
     113007 | Oeste  | Larry Fitch   |  2925.00 | Zetacorp
     113069 | Oeste  | Nancy Angelli | 31350.00 | Chen Associates
     113024 | Oeste  | Larry Fitch   |  7100.00 | Orion Corp
     112979 | Oeste  | Sue Smith     | 15000.00 | Orion Corp
     113051 | Oeste  | Larry Fitch   |  1420.00 | Midwest Systems
     112992 | Oeste  | Larry Fitch   |   760.00 | Midwest Systems
     113045 | Oeste  | Larry Fitch   | 45000.00 | Zetacorp
     113065 | Oeste  | Sue Smith     |  2130.00 | Fred Lewis Corp.
     113013 | Oeste  | Larry Fitch   |   652.00 | Midwest Systems
     113049 | Oeste  | Larry Fitch   |   776.00 | Midwest Systems
     112997 | Oeste  | Nancy Angelli |   652.00 | Peter Brothers
     113048 | Oeste  | Sue Smith     |  3750.00 | Rico Enterprises
     113034 |        | Tom Snyder    |   632.00 | Ace International
     110036 |        | Tom Snyder    | 22500.00 | Ace International
(30 rows)

R: Es perque el venedor Tom Snyder no te una oficina assignada i pertant no te una regió.

3.Mostra els noms de les empreses que han comprat productes de més de 1000$ i els noms dels representats que han fet les vendes. Ordena-ho per nom de client. No han d''apareixer resultats repetits
Select empresa, nombre from (pedidos left join clientes on  clie=num_clie left join repventas on  rep=num_empl left join productos on producto=id_producto) where productos.precio < 1000 group by empresa, nombre  having count(empresa) > 1 order by empresa;
     empresa      |    nombre     
------------------+---------------
 Acme Mfg.        | Bill Adams
 Fred Lewis Corp. | Sue Smith
 JCP Inc.         | Bill Adams
 JCP Inc.         | Paul Cruz
 Midwest Systems  | Larry Fitch
 Peter Brothers   | Nancy Angelli
(6 rows)

4.Mostrar els venedors amb un nom que no comenci ni per N ni per B, les vendes dels quals superin 25.000$
training=# select nombre
       from repventas  
       where ( nombre not like '%N%' and  nombre not like '%B%') and ventas > 25000;
   nombre    
-------------
 Mary Jones
 Sue Smith
 Sam Clark
 Dan Roberts
 Tom Snyder
 Larry Fitch
 Paul Cruz
(7 rows)
5. Mostrar tots els productes del catàleg, amb tots els seus camps, ordenats de menor a major preu, el total d''unitats que se n''han venut i quants representants diferents els han venut.
Fes que els noms de les columnes dubtoses siguin significatius.

6.Per cada client mostrar el nom del representant de ventes que te assignat , el nom del seu director si en té, i el nom del cap de l'oficina on treballa si té oficina. 
fes els noms de les columnes significatius.
 
