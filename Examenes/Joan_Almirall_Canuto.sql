1. Mostra la descripció dels productes que tenen a veure amb una "riostra" (trava en català) juntament amb el seu preu:

training=# SELECT precio, descripcion 
    from productos 
    where (descripcion ilike '% Riostra%' or descripcion ilike 'Riostra%');

 precio  |    descripcion    
---------+-------------------
 1875.00 | Riostra 2-Tm
  250.00 | Perno Riostra
   54.00 | Soporte Riostra
 1425.00 | Riostra 1-Tm
  975.00 | Riostra 1/2-Tm
  475.00 | Retenedor Riostra
(6 rows)

2. Quins codis d'oficines tenen 2 o més treballadors (representants) assignats amb una edat inferior a 50 anys? Mostra els codis d'oficina i el nímero de treballadors.

training=# SELECT oficina_rep, count (edad) as "representants"
FROM repventas
where edad <= 50
GROUP BY oficina_rep
having count(oficina_rep) >= 2;
 oficina_rep | representants 
-------------+---------------
          12 |             3
(1 row)

3. De quins productes n'hem venut en total més de 60 unitats? Mostrar, per cada producte diferent, el total d'unitats venudes, el total d'import facturat per cada producte i el número de comandes on s'ha
venut.Fes que el nom de la columna sigui significatiu.
training=# SELECT producto, sum(cant) as "comandes", sum(importe) as "import_facturat", count(num_pedido) as numero_comandes 
FROM pedidos 
GROUP BY producto
HAVING sum(cant) > 60;
 producto | comandes | import_facturat | numero_comandes 
----------+----------+-----------------+-----------------
 41002    |       64 |         4864.00 |               2
 41004    |       68 |         7956.00 |               3
(2 rows)


5. De quines fàbriques s'han venut més de 50 unitats de producte?

training=# SELECT  fab, sum(cant) as total_importe
FROM pedidos
where (cant  < 50)
GROUP BY fab
HAVING sum(cant) > 50;
 fab | total_importe 
-----+---------------
 aci |           169
 rei |            60
(2 rows)

8. Quin són els dos codis de director de representant de ventes que són directors de més representants?

training=# SELECT director, count(oficina_rep)
from repventas
WHERE director is  not null
GROUP BY director
HAVING count(oficina_rep) > 2;
 director | count 
----------+-------
      106 |     3
      104 |     3
(2 rows)


9. Quins són els codis dels clients que ens hanfet mś d'una comanda i que amb totes les comandas que han fet han pagat más de 35000?

training=# SELECT clie, count(cant), sum(importe)
from pedidos
WHERE cant > 1
training-# GROUP BY clie
training-# HAVING SUM(importe) > 35000;
 clie | count |   sum    
------+-------+----------
 2112 |     2 | 47925.00
 2103 |     4 | 35582.00
(2 rows)

10. Mostrar els identificadors dels productes de les fàbriques "fea" i "rei" que s'han venut més d'una vegada (més d'una comanda), i quantes vegades s'han venut.

training=# SELECT  producto, count(cant)
from pedidos
WHERE (fab ilike 'REI' or fab ilike 'FEA' )
group by producto
having count (cant) > 1
;
 producto | count 
----------+-------
 2a44r    |     2
 114      |     2
 2a45c    |     2
(3 rows)


 


