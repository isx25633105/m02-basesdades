```
1
```

SELECT fab, sum(cant)
FROM pedidos
GROUP BY fab
HAVING sum(cant) > 50
;

 fab | sum 
-----+-----
 aci | 223
 rei |  60
  (2 rows)


```
2
```

SELECT count(distinct id_fab) as "Numero de fabricants" 
FROM productos
WHERE (precio < 1000 and precio > 200) 
and (descripcion ilike '%or' or descripcion ilike '%ra') 
and (id_producto like '%7%' or id_producto like '%8%')
;

 Numero de fabricants 
----------------------
                    3
(1 row)


```
3
```

SELECT rep as "representants", sum(cant) as "venuts", fab as "fabrica"
FROM pedidos
GROUP BY rep, fab
ORDER BY fab ASC, venuts DESC
;

 representants | venuts | fabrica 
---------------+--------+---------
           105 |    134 | aci
           101 |     40 | aci
           103 |     24 | aci
           108 |     10 | aci
           110 |      9 | aci
           102 |      6 | aci
           108 |      1 | bic
           107 |      1 | bic
           109 |     10 | fea
           107 |     10 | fea
           106 |      6 | fea
           107 |     22 | imm
           108 |      3 | imm
           109 |      3 | imm
           102 |      2 | imm
           108 |     26 | qsa
           102 |      6 | qsa
           102 |     24 | rei
           108 |     10 | rei
           110 |      8 | rei
           106 |      7 | rei
           103 |      6 | rei
           101 |      5 | rei
(23 rows)


```
4
```

SELECT clie, sum(importe) as "Suma"
FROM pedidos
WHERE cant > 0
GROUP BY clie
HAVING sum(importe) > 35000
; 

 clie |   Suma   
------+----------
 2112 | 47925.00
 2103 | 35582.00
(2 rows)

```
5
Quins són els dos codis de director de representant de vendes 
que són directors de més representants?
```

SELECT director
FROM repventas
GROUP BY director
ORDER BY count(*) desc
LIMIT 2
;

 director 
----------
      106
      104
(2 rows)


```
6
```

SELECT descripcion, precio
FROM productos
WHERE descripcion ilike '%riostra%'
ORDER BY precio desc
;

    descripcion    | precio  
-------------------+---------
 Riostra 2-Tm      | 1875.00
 Riostra 1-Tm      | 1425.00
 Riostra 1/2-Tm    |  975.00
 Retenedor Riostra |  475.00
 Perno Riostra     |  250.00
 Soporte Riostra   |   54.00
(6 rows)


```
7
```

SELECT oficina_rep, count(num_empl)
FROM repventas
WHERE edad < 50
GROUP BY oficina_rep
HAVING count(num_empl) > 2
;

 oficina_rep | count 
-------------+-------
          12 |     3
(1 row)


```
8
```

SELECT producto, count(producto)
FROM pedidos
WHERE fab ilike 'rei' or fab ilike 'fea'
GROUP BY producto
HAVING count(producto) > 1
; 

 producto | count 
----------+-------
 2a44r    |     2
 114      |     2
 2a45c    |     2
(3 rows)


```
9
```

SELECT producto, SUM(cant) as "Unitats que shan venut" ,
SUM(importe) as "Import total",
COUNT(*) as "Numero de comandes" 
FROM pedidos
GROUP BY producto
HAVING SUM(cant) > 60
;

 producto | Unitats que shan venut | Import total | Numero de comandes 
----------+------------------------+--------------+--------------------
 41002    |                     64 |      4864.00 |                  2
 41004    |                     68 |      7956.00 |                  3
(2 rows)


```
10
```

SELECT nombre, (ventas-cuota) as "SuperacioQuota", ROUND(ventas/cuota*100,2) as "SuperacioQuotaPercentatge"
FROM repventas
WHERE cuota IS NOT NULL
;
    nombre     | SuperacioQuota | SuperacioQuotaPercentatge 
---------------+----------------+---------------------------
 Bill Adams    |       17911.00 |                    105.12
 Mary Jones    |       92725.00 |                    130.91
 Sue Smith     |      124050.00 |                    135.44
 Sam Clark     |       24912.00 |                    109.06
 Bob Smith     |      -57406.00 |                     71.30
 Dan Roberts   |        5673.00 |                    101.89
 Larry Fitch   |       11865.00 |                    103.39
 Paul Cruz     |       11775.00 |                    104.28
 Nancy Angelli |     -113958.00 |                     62.01
