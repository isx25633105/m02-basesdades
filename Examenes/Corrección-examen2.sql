1.Per cada oficina llista'n' la ciutat, les vendes segons la taula oficinas i les vendes segons la taula pedidos. Posa noms significatius a les dues columnes de vendes.
 training=# select oficina, ciudad, sum(importe) as pedidos_ventas, oficinas.ventas as oficinas_ventas from (repventas left join pedidos on num_empl=rep left join oficinas on oficina=oficina_rep) where oficina is not null group by oficina, ciudad;
 oficina |   ciudad    | pedidos_ventas | oficinas_ventas 
---------+-------------+----------------+-----------------
      11 | New York    |       40063.00 |       692637.00
      12 | Chicago     |       29328.00 |       735042.00
      13 | Atlanta     |       39327.00 |       367911.00
      21 | Los Angeles |       81409.00 |       835915.00
      22 | Denver      |       34432.00 |       186042.00
(5 rows)
2.Per a totes les comandes, visualitzar-ne el número de comanda, la regió de l''oficina del representatn de vendes que l''ha fet, l''import i el nom de l''empresa que ha comprat el producte.
Mostrar-ho ordenat regió. Respon a sota dels resultats: perquè hi ha dues comandes per a les quals la regió surt en blanc? 
select num_pedido, region, nombre, importe, empresa
from (pedidos left join repventas on rep=num_empl left join oficinas on oficina=oficina_rep left join clientes on clie=num_clie)
Order by region
;
 num_pedido | region |    nombre     | importe  |      empresa      
------------+--------+---------------+----------+-------------------
     113042 | Este   | Dan Roberts   | 22500.00 | Ian & Schmidt
     113012 | Este   | Bill Adams    |  3745.00 | JCP Inc.
     112989 | Este   | Sam Clark     |  1458.00 | Jones Mfg.
     112968 | Este   | Dan Roberts   |  3978.00 | First Corp.
     112963 | Este   | Bill Adams    |  3276.00 | Acme Mfg.
     113058 | Este   | Mary Jones    |  1480.00 | Holm & Landis
     112983 | Este   | Bill Adams    |   702.00 | Acme Mfg.
     113027 | Este   | Bill Adams    |  4104.00 | Acme Mfg.
     112975 | Este   | Paul Cruz     |  2100.00 | JCP Inc.
     113055 | Este   | Dan Roberts   |   150.00 | Holm & Landis
     113003 | Este   | Mary Jones    |  5625.00 | Holm & Landis
     112987 | Este   | Bill Adams    | 27500.00 | Acme Mfg.
     113057 | Este   | Paul Cruz     |   600.00 | JCP Inc.
     112961 | Este   | Sam Clark     | 31500.00 | J.P. Sinclair
     113062 | Oeste  | Nancy Angelli |  2430.00 | Peter Brothers
     112993 | Oeste  | Sue Smith     |  1896.00 | Fred Lewis Corp.
     113007 | Oeste  | Larry Fitch   |  2925.00 | Zetacorp
     113069 | Oeste  | Nancy Angelli | 31350.00 | Chen Associates
     113024 | Oeste  | Larry Fitch   |  7100.00 | Orion Corp
     112979 | Oeste  | Sue Smith     | 15000.00 | Orion Corp
     113051 | Oeste  | Larry Fitch   |  1420.00 | Midwest Systems
     112992 | Oeste  | Larry Fitch   |   760.00 | Midwest Systems
     113045 | Oeste  | Larry Fitch   | 45000.00 | Zetacorp
     113065 | Oeste  | Sue Smith     |  2130.00 | Fred Lewis Corp.
     113013 | Oeste  | Larry Fitch   |   652.00 | Midwest Systems
     113049 | Oeste  | Larry Fitch   |   776.00 | Midwest Systems
     112997 | Oeste  | Nancy Angelli |   652.00 | Peter Brothers
     113048 | Oeste  | Sue Smith     |  3750.00 | Rico Enterprises
     113034 |        | Tom Snyder    |   632.00 | Ace International
     110036 |        | Tom Snyder    | 22500.00 | Ace International
(30 rows)

R: Es perque el venedor Tom Snyder no te una oficina assignada i pertant no te una regió.

3.Mostra els noms de les empreses que han comprat productes de més de 1000$ i els noms dels representats que han fet les vendes. Ordena-ho per nom de client. No han d''apareixer resultats repetits
Select empresa, nombre from (pedidos left join clientes on  clie=num_clie left join repventas on  rep=num_empl left join productos on producto=id_producto) where productos.precio < 1000 group by empresa, nombre  having count(empresa) > 1 order by empresa;
     empresa      |    nombre     
------------------+---------------
 Acme Mfg.        | Bill Adams
 Fred Lewis Corp. | Sue Smith
 JCP Inc.         | Bill Adams
 JCP Inc.         | Paul Cruz
 Midwest Systems  | Larry Fitch
 Peter Brothers   | Nancy Angelli
(6 rows)

*Forma del profesor 
training=# Select empresa, descripcion, precio, nombre 
from pedidos left join clientes on num_clie=clie
left join productos on fab=id_fab and producto = id_producto
left join repventas on rep=num_empl
where precio > 1000
order by empresa;
      empresa      |  descripcion   | precio  |    nombre     
-------------------+----------------+---------+---------------
 Ace International | Montador       | 2500.00 | Tom Snyder
 Acme Mfg.         | Extractor      | 2750.00 | Bill Adams
 Chen Associates   | Riostra 1-Tm   | 1425.00 | Nancy Angelli
 Holm & Landis     | Riostra 2-Tm   | 1875.00 | Mary Jones
 Ian & Schmidt     | Bisagra Dcha.  | 4500.00 | Dan Roberts
 J.P. Sinclair     | Bisagra Izqda. | 4500.00 | Sam Clark
 Orion Corp        | Montador       | 2500.00 | Sue Smith
 Rico Enterprises  | Riostra 2-Tm   | 1875.00 | Sue Smith
 Zetacorp          | Bisagra Dcha.  | 4500.00 | Larry Fitch
(9 rows)

4.Mostrar els venedors amb un nom que no comenci ni per N ni per B, les vendes dels quals superin 25.000$
training=# select nombre
       from repventas  
       where ( nombre not like '%N%' and  nombre not like '%B%') and ventas > 25000;
   nombre    
-------------
 Mary Jones
 Sue Smith
 Sam Clark
 Dan Roberts
 Tom Snyder
 Larry Fitch
 Paul Cruz
(7 rows)
5. Mostrar tots els productes del catàleg, amb tots els seus camps, ordenats de menor a major preu, el total d''unitats que se n''han venut i quants representants diferents els han venut.
Fes que els noms de les columnes dubtoses siguin significatius.
Select productos.*, sum(cant) as RecompteUnitats, count(distinct(rep)) as RecompteRepresentants
from productos join pedidos on id_fab=fab and id_producto=producto
Group by id_fab, id_producto, descripcion, precio, existencias order by precio;
 id_fab | id_producto |    descripcion    | precio  | existencias | recompteunit
ats | recompterepresentants 
--------+-------------+-------------------+---------+-------------+-------------
----+-----------------------
 aci    | 4100x       | Ajustador         |   25.00 |          37 |             
 30 |                     2
 aci    | 41002       | Articulo Tipo 2   |   76.00 |         167 |             
 64 |                     2
 rei    | 2a45c       | V Stago Trinquete |   79.00 |         210 |             
 32 |                     2
 aci    | 41003       | Articulo Tipo 3   |  107.00 |         207 |             
 35 |                     1
 aci    | 41004       | Articulo Tipo 4   |  117.00 |         139 |             
 68 |                     2
 fea    | 112         | Cubierta          |  148.00 |         115 |             
 10 |                     1
 fea    | 114         | Bancada Motor     |  243.00 |          15 |             
 16 |                     2
 rei    | 2a44g       | Pasador Bisagra   |  350.00 |          14 |             
  6 |                     1
 qsa    | xk47        | Reductor          |  355.00 |          38 |             
 28 |                     2
 bic    | 41003       | Manivela          |  652.00 |           3 |             
  2 |                     2
 imm    | 773c        | Riostra 1/2-Tm    |  975.00 |          28 |             
  3 |                     1
 imm    | 775c        | Riostra 1-Tm      | 1425.00 |           5 |             
 22 |                     1
 imm    | 779c        | Riostra 2-Tm      | 1875.00 |           9 |             
  5 |                     2
 aci    | 4100z       | Montador          | 2500.00 |          28 |             
 15 |                     2
 aci    | 4100y       | Extractor         | 2750.00 |          25 |             
 11 |                     1
 rei    | 2a44l       | Bisagra Izqda.    | 4500.00 |          12 |             
  7 |                     1
 rei    | 2a44r       | Bisagra Dcha.     | 4500.00 |          12 |             
 15 |                     2
(17 rows)

6.Per cada client mostrar el nom del representant de ventes que
Select empresa, rep.nombre as NomRepresentatn, dir.nombre as NomDirector, dirofi.nombre as NomDirectorOficina
From clientes left join repventas as rep on rep_cli=num_empl
left join repventas as dir on rep.director=dir.num_empl
left join oficinas on rep.oficina_rep=oficina
left join repventas as dirofi on oficinas.dir=dirodi.num_empl;
     empresa      | nomrepresentatn | nomdirector | nomdirectoroficina 
-------------------+-----------------+-------------+--------------------
 JCP Inc.          | Paul Cruz       | Bob Smith   | Bob Smith
 First Corp.       | Dan Roberts     | Bob Smith   | Bob Smith
 Acme Mfg.         | Bill Adams      | Bob Smith   | Bill Adams
 Carter & Sons     | Sue Smith       | Larry Fitch | Larry Fitch
 Ace International | Tom Snyder      | Dan Roberts | 
 Smithson Corp.    | Dan Roberts     | Bob Smith   | Bob Smith
 Jones Mfg.        | Sam Clark       |             | Sam Clark
 Zetacorp          | Larry Fitch     | Sam Clark   | Larry Fitch
 QMA Assoc.        | Paul Cruz       | Bob Smith   | Bob Smith
 Orion Corp        | Sue Smith       | Larry Fitch | Larry Fitch
 Peter Brothers    | Nancy Angelli   | Larry Fitch | Larry Fitch
 Holm & Landis     | Mary Jones      | Sam Clark   | Sam Clark
 J.P. Sinclair     | Sam Clark       |             | Sam Clark
 Three-Way Lines   | Bill Adams      | Bob Smith   | Bill Adams
 Rico Enterprises  | Sue Smith       | Larry Fitch | Larry Fitch
 Fred Lewis Corp.  | Sue Smith       | Larry Fitch | Larry Fitch
 Solomon Inc.      | Mary Jones      | Sam Clark   | Sam Clark
 Midwest Systems   | Larry Fitch     | Sam Clark   | Larry Fitch
 Ian & Schmidt     | Bob Smith       | Sam Clark   | Bob Smith
 Chen Associates   | Paul Cruz       | Bob Smith   | Bob Smith
 AAA Investments   | Dan Roberts     | Bob Smith   | Bob Smith
(21 rows)
7.
Select num_pedido, fab, producto, id_fab, id_producto
from pedidos left join productos on fab=id_fab and producto=id_producto
where id_fab is null;
 num_pedido | fab | producto | id_fab | id_producto 
------------+-----+----------+--------+-------------
     113051 | qsa | k47      |        | 
(1 row)

8.
select ciudad, count(num_pedido)
from pedidos left join productos on producto=id_producto and fab=id_fab
left join repventas on rep=num_empl
left join oficinas on oficina_rep=oficina
where descripcion ilike '%reostra%' OR descricion 
ilike 'V%'
group by ciudad;
   ciudad    | count 
-------------+-------
 Los Angeles |     1
             |     1
(2 rows)
9.
