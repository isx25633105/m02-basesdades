
--1. Mostar les ventes individuals dels productes dels fabricants 'aci' i 'rei' que comencin per 'Bisagra' o 'Articulo'. Mostrar també el total venut d'aquests productes.
SELECT 'Venda:' as "Venda/Total", num_pedido, descripcion, fab, producto,  cant, fecha_pedido as "data"
FROM pedidos JOIN productos on fab=id_fab and producto=id_producto
where fab ilike 'aci' or fab ilike 'rei'
UNION
SELECT 'TOTAL:', 0, descripcion, fab, producto, sum(cant), current_timestamp
FROM pedidos JOIN productos on fab=id_fab and producto=id_producto
where fab ilike 'aci' or fab ilike 'rei'
group by descripcion, fab, producto

ORDER BY fab, producto, "data";


--2. Mostrar les ventes individuals fetes pels venedors de New York i de Chicago que superin els 2500€. Mostrar també el total de ventes de cada oficina.
SELECT ciudad, 'Venda:' as operacio, producto, fab, cant, importe
from pedidos
join repventas on rep=num_empl
join oficinas on oficina_rep=oficina

where oficina_rep=11 or oficina_rep=12
UNION
SELECT ciudad, 'TOTAL:', NULL, NULL , NULL, sum(importe)
from pedidos join repventas on rep=num_empl
join oficinas on oficina_rep=oficina
where oficina_rep=11 or oficina_rep=12
GROUP BY ciudad, oficina_rep
ORDER BY ciudad, operacio desc;


--posant la columna cant com a text:

SELECT ciudad, 'Venda:' as operacio, producto, fab, cant::TEXT, importe
from pedidos
join repventas on rep=num_empl
join oficinas on oficina_rep=oficina

where oficina_rep=11 or oficina_rep=12
UNION
SELECT ciudad, 'TOTAL:', '-----', '---' , '---', sum(importe)
from pedidos join repventas on rep=num_empl
join oficinas on oficina_rep=oficina
where oficina_rep=11 or oficina_rep=12
GROUP BY ciudad, oficina_rep
ORDER BY ciudad, operacio desc;




--3. Mostrar quantes ventes ha fet cada venedor, la mitjana de numero de ventes dels venedors de cada oficina i el numero de ventes total.

select 'Vendes representant:' as Dada, ciudad, nombre, count(num_pedido) as "Total Comandes", sum(importe) as "Total Vendes"
from pedidos join repventas on rep=num_empl left join oficinas on oficina_rep=oficina
group by ciudad, nombre
UNION
select '-Mitjana per oficina:', ciudad, NULL, count(num_pedido), avg(importe)
from pedidos join repventas on rep=num_empl left join oficinas on oficina_rep=oficina
WHERE ciudad IS NOT NULL
group by ciudad

UNION
select '--TOTAL GENERAL:', NULL, NULL , count(num_pedido), sum(importe)
from pedidos
order by ciudad, dada desc;

----millor:
select 'vendedores: ', nombre, ciudad, repventas.ventas from repventas
left join oficinas on oficina_rep=oficina

union

select 'media: ', NULL, ciudad, avg(repventas.ventas)
from oficinas
right join repventas on oficina=oficina_rep
group by ciudad

union

select 'TOTAL GENERAL: ', NULL, NULL, sum(repventas.ventas)
from repventas

order by ciudad, nombre asc;


---- fet a classe:
select 'vendedores: ', nombre, ciudad, repventas.ventas from repventas
left join oficinas on oficina_rep=oficina

union

select 'media: ', NULL, ciudad, avg(repventas.ventas)
from oficinas
right join repventas on oficina=oficina_rep
group by ciudad

union

select 'TOTAL GENERAL: ', NULL, NULL, sum(repventas.ventas)
from repventas

order by ciudad, nombre asc;

--4. Mostrar les compres de productes de la fabrica 'aci' que han fet els clients del Bill Adams i el Dan Roberts. Mostrar també l'import total per cada client.

select 'Compra:' as op, empresa, nombre, num_pedido, descripcion, fab, producto, cant, importe
from pedidos join clientes on clie=num_clie
join repventas on rep_clie=num_empl
left join productos on fab = id_fab and producto=id_producto
where (num_empl=105 or num_empl=101) and fab ilike 'aci'

UNION

select 'TOTAL:', empresa, NULL, NULL, NULL, NULL, NULL, NULL, sum(importe)
from pedidos join clientes on clie=num_clie
join repventas on rep_clie=num_empl
left join productos on fab = id_fab and producto=id_producto
where (num_empl=105 or num_empl=101) and fab ilike 'aci'
group by empresa

order by empresa, op;

-----fet a classe:
select 'comanda:' as concepte, num_pedido, descripcion, fab, cant, importe, empresa, nombre
from pedidos left join productos on fab=id_fab and producto=id_producto
join clientes on clie=num_clie
join repventas on rep_clie=num_empl
where fab ilike 'aci' and (num_empl=105 or num_empl=101)
UNION
select 'total client:', NULL, NULL, NULL, NULL, sum(importe), empresa, NULL
from pedidos
join clientes on clie=num_clie
join repventas on rep_clie=num_empl
where fab ilike 'aci' and (num_empl=105 or num_empl=101)
group by empresa
order by empresa, concepte asc;


--5. Mostrar el total de ventes de cada oficina i el total de ventes de cada regió
select ciudad, region, ventas
from oficinas
UNION
select 'TOTAL REGIÓ:',region,sum(ventas)
from oficinas
group by region
order by region, ciudad;

-----a classe:
select 'ventas por oficina:' as "concepte al qual m'estic referint", ciudad, region, ventas
from oficinas

union

select 'ventas por region: ', NULL, region, sum(ventas) from oficinas
group by region
order by 3, 1;

--6. Mostrar els noms dels venedors de cada ciutat i el numero total de venedors per ciutat

select nombre, ciudad, NULL as "Recompte"
from repventas join oficinas on oficina_rep=oficina
UNION
select '--RECOMPTE:', ciudad, count(*)
from repventas join oficinas on oficina_rep=oficina
group by ciudad
order by ciudad, "Recompte" desc;


------ a classe:
select 'venedor:' as concepte, ciudad, nombre, NULL as total
from repventas join oficinas on oficina_rep=oficina

union

select 'recompte per ciutat:', ciudad, NULL, count (num_empl)
from repventas join oficinas on oficina_rep=oficina
group by ciudad

order by ciudad, concepte desc;

--7. Mostrat els noms dels clients de cada ciutat i el numero total de clients per ciutat.

select 'client:' as concepte,  ciudad, empresa, 0 as TOTAL
from clientes join repventas on rep_clie=num_empl
left join oficinas on oficina_rep=oficina
UNION
select 'TOTAL CIUTAT:', ciudad, NULL, count(*)
from clientes join repventas on rep_clie=num_empl
left join oficinas on oficina_rep=oficina
group by ciudad
order by ciudad, TOTAL;



--8. Mostrat els noms dels treballadors que son -caps- d'algú, els noms dels seus -subordinats- i el numero de treballadors que té assignat cada cap.
select rvcaps.nombre as "Nom cap", rvsub.nombre as "Nom subordinada", NULL as "Total subordinades per cap"
from repventas as rvsub join repventas as rvcaps on rvsub.director = rvcaps.num_empl
UNION
select rvcaps.nombre as "Nom cap", 'Total subordinades:', count(*)
from repventas as rvsub join repventas as rvcaps on rvsub.director = rvcaps.num_empl
group by rvcaps.nombre
order by 1, 3 desc;
